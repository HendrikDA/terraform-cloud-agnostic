# Code Examples for the Bachelor-Thesis "Analyzing the Extent to Which Terrafor m Can Model Cloud-Agnostic Infrastructure"

## Structure of the Repository

At the top level of the repository, you will find a PDF of my Bachelor Thesis.

The code examples for the first use case in chapter 5 - Methodology - can be found within the folder named use-case-1. The code examples for the second use case, also in chapter 5, can be found under use-case-2.

## How to execute the code

If you wish to execute the code examples, you must first have the Terraform CLI installed on your local system. On top of that, you will additionally need to provide your own authentication within the code. You will need to do the following for each provider

### AWS

Within the `terraform.tfvars.dist` file, you will need to add in your profile alias and **rename** the file to `terraform.tfvars`, as the .dist file is meant as a template. This is the alias used within you AWS configuration under your local `~/.aws` folder

### Azure

In order to authenticate my Azure profile, I used the command `az login`, which requires the Azure CLI. This will authenticate my profile via the web browser, allowing me to make API calls towards my account.

## Contact Me

If you wish to contact me for whatever reason, you can shoot me an email `hendrik.grubz@gmail.com`
