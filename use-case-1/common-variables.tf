# Common variables shared between AWS and Azure

# Define the application name
variable "app_name" {
  description = "Application name"
  type        = string
  default     = "simple_web_server"
}

# Define application environment
variable "app_environment" {
  description = "Application environment"
  type        = string
  default     = "dev"
}

# AWS provider information
variable "aws_profile" {
  description = "The profile used for AWS"
  type        = string
}

variable "aws_region" {
  type        = string
  description = "AWS Region for the web server"
  default     = "eu-central-1"
}