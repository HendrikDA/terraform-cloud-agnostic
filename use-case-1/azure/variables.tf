# Variables used for the Azure configuration

variable "resource_group_location" {
  type        = string
  description = "Location of Resource Group"
  default     = "Germany West Central"
}

variable "azure_subnet_cidr" {
  type        = string
  description = "Subnet CIDR"
  default     = "10.0.2.0/24"
}

# Define the application name
variable "app_name" {
  description = "Application name"
  type        = string
  default     = "simple_web_server"
}

# Define application environment
variable "app_environment" {
  description = "Application environment"
  type        = string
  default     = "dev"
}