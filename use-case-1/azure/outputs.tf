# Output variables for Azure

output "azure_public_ip" {
  description = "The public IP address of the Azure web server"
  value       = azurerm_public_ip.azure-web-ip.ip_address
}