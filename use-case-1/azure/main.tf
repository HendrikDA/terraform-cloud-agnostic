provider "azurerm" {
  features {}
}

# Manages a Resource Group
resource "azurerm_resource_group" "azure-resource-group" {
  name     = "${var.app_name}-${var.app_environment}-resource-group"
  location = var.resource_group_location
}

# Filter network traffic to and from Azure resources in an Azure virtual network
resource "azurerm_network_security_group" "azure-web-network-security-group" {
  name                = "${var.app_name}-${var.app_environment}-web-network-security-group"
  location            = azurerm_resource_group.azure-resource-group.location
  resource_group_name = azurerm_resource_group.azure-resource-group.name

  security_rule {
    name                       = "web-server"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# Manages a virtual network including any configured subnets
resource "azurerm_virtual_network" "azure-virtual-network" {
  name                = "vnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.azure-resource-group.location
  resource_group_name = azurerm_resource_group.azure-resource-group.name
}
# Manages a subnet. Subnets represent network segments within the IP space defined by the virtual network
resource "azurerm_subnet" "azure-subnet" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.azure-resource-group.name
  virtual_network_name = azurerm_virtual_network.azure-virtual-network.name
  address_prefixes     = ["10.0.2.0/24"]
}

# Manages a Network Interface
resource "azurerm_network_interface" "azure-network-interface" {
  name                = "example-nic"
  location            = azurerm_resource_group.azure-resource-group.location
  resource_group_name = azurerm_resource_group.azure-resource-group.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.azure-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.azure-web-ip.id
  }
}

# Manages the association between a Network Interface and a Network Security Group.
resource "azurerm_network_interface_security_group_association" "azure-security-group-association" {
  network_interface_id      = azurerm_network_interface.azure-network-interface.id
  network_security_group_id = azurerm_network_security_group.azure-web-network-security-group.id
}

# Manages a Public IP Address
resource "azurerm_public_ip" "azure-web-ip" {
  name                = "${var.app_name}-${var.app_environment}-web-ip"
  location            = azurerm_resource_group.azure-resource-group.location
  resource_group_name = azurerm_resource_group.azure-resource-group.name
  allocation_method   = "Static"
}

# Manages a Linux Virtual Machine
resource "azurerm_linux_virtual_machine" "azure-linux-vm" {
  name                            = "example-machine"
  resource_group_name             = azurerm_resource_group.azure-resource-group.name
  location                        = azurerm_resource_group.azure-resource-group.location
  size                            = "Standard_B2s"
  admin_username                  = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.azure-network-interface.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
}

# Manages a Virtual Machine Extension to provide post deployment configuration and run automated tasks
resource "azurerm_virtual_machine_extension" "azure-vm-script" {
  name                 = "azure-vm-script"
  virtual_machine_id   = azurerm_linux_virtual_machine.azure-linux-vm.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.0"

  settings = <<SETTINGS
    {
      "commandToExecute" : "echo 'Hello, World from Azure' > index.html && nohup busybox httpd -f -p 8080 &" 
    }
    SETTINGS
}
