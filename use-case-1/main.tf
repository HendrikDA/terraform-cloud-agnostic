
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

provider "azurerm" {
  features {}
}

module "aws-web-server" {
  source      = "./aws"
}

module "azure-web-server" {
  source = "./azure"
}

output "aws_outputs" {
  value = module.aws-web-server
}

output "azure_outputs" {
  value = module.azure-web-server
}