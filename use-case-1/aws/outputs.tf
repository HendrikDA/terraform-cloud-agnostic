# Output variables for AWS

output "aws_public_ip" {
  description = "The public IP address of the AWS web server"
  value       = aws_instance.simple-web-server.public_ip
}