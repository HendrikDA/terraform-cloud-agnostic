# Create an EC2 Instance
resource "aws_instance" "simple-web-server" {
  ami                    = "ami-9a380b87"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.instance-security-group.id]
  user_data              = <<-EOF
                #!/bin/bash
                echo "Hello, World from AWS" > index.html
                nohup busybox httpd -f -p 8080 &
                EOF
}

# Create a security group for the EC2 instance
resource "aws_security_group" "instance-security-group" {
  name = "simple-web-server-instance"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
  }
}

