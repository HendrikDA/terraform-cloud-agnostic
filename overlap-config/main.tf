# Create an AWS security group using Azure settings
resource "aws_security_group" "instance-security-group" {
  name = "agnostic-security-group"

  security_rule {
    name                       = "web-server"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}