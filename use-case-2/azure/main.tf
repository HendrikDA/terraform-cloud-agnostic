provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "db-resource-group" {
  name     = "db-resource-group"
  location = "Germany West Central"
}

resource "azurerm_postgresql_server" "azure-db-server" {
  name                = "azure-db-server-terraform"
  location            = azurerm_resource_group.db-resource-group.location
  resource_group_name = azurerm_resource_group.db-resource-group.name

  sku_name   = "B_Gen5_2"
  storage_mb = 5120

  administrator_login          = "hendrikroot"
  administrator_login_password = "Test1234--"
  version                      = "10"
  ssl_enforcement_enabled      = false
}

resource "azurerm_postgresql_database" "terraform_database" {
  name                = "azure-database"
  resource_group_name = azurerm_resource_group.db-resource-group.name
  server_name         = azurerm_postgresql_server.azure-db-server.name
  charset             = "UTF8"
  collation           = "en-US"
}

output "db_address" {
  description = "The public address to connect to the DB"
  value       = "${azurerm_postgresql_server.azure-db-server.administrator_login}@${azurerm_postgresql_server.azure-db-server.name}.postgres.database.azure.com"
}