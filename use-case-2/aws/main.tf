resource "aws_db_instance" "aws-database" {
  allocated_storage    = 20
  engine               = "postgres"
  engine_version       = "10"
  instance_class       = "db.t2.micro"
  name                 = "aws-database"
  character_set_name   = "UTF8"
  username             = "root"
  password             = "Test1234--"
  publicly_accessible  = true
  vpc_security_group_ids = [aws_security_group.database-security-group.id]
}

# Create a security group for the Database
resource "aws_security_group" "database-security-group" {
  name        = "database-security-grop"
  description = "Security group for the database"
  vpc_id      = "vpc-92fd24f8"

  ingress {
    description = "Allow TCP Connections on port 5432"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Allow DB Connections"
  }
}

output "aws_public_ip" {
  description = "The public IP address of the AWS database"
  value       = aws_db_instance.aws-database.address
}

output "aws_port" {
  description = "The port of the AWS database"
  value       = aws_db_instance.aws-database.port
}