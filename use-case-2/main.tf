
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

provider "azurerm" {
  features {}
}

module "aws-database" {
  source = "./aws"
}

module "azure-database" {
  source = "./azure"
}

output "aws_outputs" {
  value = module.aws-database
}

output "azure_outputs" {
  value = module.azure-database
}